package com.davidtiago.androidsplash.randompicture.data

import com.davidtiago.androidsplash.randompicture.data.remote.RandonPictureService
import javax.inject.Inject

interface RandomPictureRepository {
    suspend fun getRandomPicture(): RandomPicture
}

class RemoteRandomPictureRepository @Inject constructor(
    private val randomPictureService: RandonPictureService
) : RandomPictureRepository {
    override suspend fun getRandomPicture() = randomPictureService.get()
}
