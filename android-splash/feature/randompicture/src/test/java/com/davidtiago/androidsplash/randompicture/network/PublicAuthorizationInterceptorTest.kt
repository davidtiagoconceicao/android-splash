package com.davidtiago.androidsplash.randompicture.network

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

const val SAMPLE_API_KEY = "SAMPLE_API_KEY"

class PublicAuthorizationInterceptorTest {
    private val interceptor = PublicAuthorizationInterceptor(SAMPLE_API_KEY)
    private val server = MockWebServer()

    @BeforeEach
    fun setup() {
        server.enqueue(MockResponse())
        val okHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(interceptor)
            .build()
        okHttpClient.newCall(
            Request.Builder()
                .url(
                    server.url("/")
                )
                .build()
        ).execute()
    }

    @Test
    fun interceptShouldAddClientId() {
        val recordedRequest = server.takeRequest()
        assertEquals(
            expected = "Client-ID $SAMPLE_API_KEY",
            actual = recordedRequest.getHeader("Authorization")
        )
    }

    @AfterEach
    fun tearDown() = server.shutdown()

}
