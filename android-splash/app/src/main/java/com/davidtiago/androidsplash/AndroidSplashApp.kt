package com.davidtiago.androidsplash

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AndroidSplashApp : Application()
