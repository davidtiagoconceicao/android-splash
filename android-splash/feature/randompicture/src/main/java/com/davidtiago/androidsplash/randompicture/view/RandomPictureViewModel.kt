package com.davidtiago.androidsplash.randompicture.view

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.davidtiago.androidsplash.randompicture.data.RandomPicture
import com.davidtiago.androidsplash.randompicture.data.RandomPictureRepository
import kotlinx.coroutines.launch
import java.io.IOException

class RandomPictureViewModel @ViewModelInject constructor(
    private val repository: RandomPictureRepository
) : ViewModel() {
    private val viewStatePrivate: MutableLiveData<RandomPictureViewState> by lazy {
        MutableLiveData<RandomPictureViewState>().also {
            refresh()
        }
    }
    val viewState: LiveData<RandomPictureViewState>
        get() = viewStatePrivate

    fun refresh() {
        viewModelScope.launch {
            viewStatePrivate.postValue(RandomPictureViewState.Loading)
            try {
                val randomPicture = repository.getRandomPicture()
                val state = RandomPictureViewState.Loaded(randomPicture)
                viewStatePrivate.postValue(state)
            } catch (e: IOException) {
                viewStatePrivate.postValue(RandomPictureViewState.LoadError)
            }
        }
    }
}

sealed class RandomPictureViewState {
    class Loaded(val randomPicture: RandomPicture) : RandomPictureViewState()
    object Loading : RandomPictureViewState()
    object LoadError : RandomPictureViewState()
}
